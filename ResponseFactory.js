const logger = require('winston');

class ResponseFactory {
  constructor(errorList) {
    this.errorList = errorList;
  }

  ok(res, result) {
    return res.status(200).json(result);
  }

  error(res, error) {
    if (this.errorList.indexOf(error.message) !== -1) {
      return res.status(400).json(error.message);
    }

    logger.error('unhandled error: ', error);
    return res.status(500).json('SERVER_ERROR');
  }
}

module.exports = ResponseFactory;